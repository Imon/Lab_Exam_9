<?php
use App\BITM\SEIP106607\Writer\Writer;
include_once ("../../vendor/autoload.php");


$user_name = $_POST['user_name'];
$writer_name = $_POST['writer_name'];

$name = new Writer();
if(isset($_POST['id'])){
    $id = $_POST['id'];
    $name->update($id, $user_name, $writer_name);
}else{
    $name->store($user_name, $writer_name);
}


