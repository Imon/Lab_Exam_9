<?php

use App\BITM\SEIP106607\Writer\Writer;
include_once ("../../vendor/autoload.php");

$name = new Writer();
$names = $name->index();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    
    
    <body>
        <a href="01simple-download-xlsx.php">Excel</a>
        <table border='1'>
            <thead>
                <tr>
                    <td>SL.NO></td>
                    <td>User Name</td>
                    <td>Writer Name</td>
                    <td colspan="2">Action</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($names as $info){
                    $i++;
                
                ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $info['user_name'];?></td>
                    <td><?php echo $info['writer_name'];?></td>
                    <td>
                        <form action="edit.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $info['id'];?>"/>
                            <input type="submit" value="Edit">
                            
                        </form>
                    </td>
                     <td>
                         <form action="delete.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $info['id'];?>"/>
                            <input type="submit" value="Delete">
                            
                        </form>
                    </td>
                    
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <a href="../../index.php"><input type="submit" value="Back To Home"/></a>
        <a href="create.php"><input type="submit" value="Create New"/></a>
    </body>
</html>