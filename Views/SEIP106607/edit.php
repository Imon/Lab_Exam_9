<?php

use App\BITM\SEIP106607\Writer\Writer;
include_once ("../../vendor/autoload.php");

$id = $_POST['id'];
$name = new Writer();
$edit = $name->edit($id);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="store.php" method="post">
            <fieldset>
                <legend>Edit Favourite Writer's</legend>
                <label>Name</label><br/>
                <input type="text" name="user_name" value="<?php echo $edit['user_name'] ?>"/><br/><br/>
                <label>Favourite Writer Name</label><br/>
                <input type="text" name="writer_name" value="<?php echo $edit['writer_name'] ?>"/><br/><br/>
                <input type="hidden" name="id" value="<?php echo $edit['id'] ?>"/>
                <input type="submit" value="update"/>
            </fieldset>
        </form>
    </body>
</html>

